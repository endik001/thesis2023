Containing supplementary materials included in the thesis entitled Gut rebalancing acts: evaluating the role of prebiotics in the recovery of antibiotic-perturbed infant gut microbiota

The supplementary materials are grouped by chapter number. Please refer to specific chapter branch.
